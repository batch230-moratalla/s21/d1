console.log("Hello Tuesday!");

// S20 review

		// 0	  // true		1
	// after change of value
		// 1	  // true		2
	// after change of value
		// 2 	  // true		3
	// after change of value
		// 3 	  // true		4
	// after change of value
		// 4 	  // true		5
	// after change of value
		// 5	  // true
for(let number = 0; number < 10; number++){

	if(number == 2){ //false // false // true - display message // false // false
		console.log("Number 2 is found, skip next line of codes then continue loop");
		continue;
		console.log('New Message'); //lahat ng baba ng continue iiskip nya
		// when this condition is met the loop will continue to "increment" or change of value
	}
	// console.log('New Message');

	if(number != 2){ // display // display // disregarded // display // display // display
		console.log(number);
	}

	if(number == 5){ // false // false // disregarded // false // false // true
		console.log("Stop the loop at number 5");
		break;
	}
}

// continue / break / return - similar behaviour

/*
	1. Initialize a value
	2. Checks the condition
	3. Runs the statemaent inside the loop if the condition is true
	4. change of value (Increment or Decrement)
		- go back to steps 2
*/

console.log("---------------------------------------")

// Reassigning with concatenation of string

let myString = "doooooom";
let letterO = "";

for(let i=0; i<myString.length; i++){
	if(myString[i] == 'o'){
		//  empltyString("")  +  o
		//  		o 		  +  o
		//			oo 	 	  +  o
		//			ooo 	  +  o
		//			oooo 	  +  o
		//			ooooo 	  
		letterO = letterO + myString[i]; // concantenate
	}
}
console.log(letterO);

console.log("---------------------------------------")
// [SECTION]  Array

// An array in programming is simple a list of data. Let's write the example earlier

let studentNumberA = '2020-1923';
let studentNumberB = '2020-1924';
let studentNumberC = '2020-1925';
let studentNumberD = '2020-1926';
let studentNumberE = '2020-1927';

let studentNumbers = ['2020-1923', '2020-1924', '2020-1925', '2020-1926', '2020-1927'];

// Common examples of arrays
// index 	  0 	1 	  2 	3
let grades = [98.5, 94.3, 89.2, 90.1, 99];
// index 				0 		1 		2 	 	 3
let computerBrands= ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu']

// Possible use of an array but is not recommended
let mixedArr = [12, 'Asus', null, undefined, {}];

console.log(grades);
console.log(computerBrands);
console.log(mixedArr);

// Alternative ways to write arrays
let myTask = [
		'drink HTML',
		'eat javascript',
		'inhale css',
		'bake bootstrap'
	]
console.log(myTask);

// Creating an array with values from variables
let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "Jakarta";

let cities = [city1, city2, city3];
console.log(cities);

console.log("---------------------------------------")
// [SECTION]  Length Property

// The ".length" property allows us to get and set the total number of items in an array
console.log(myTask.length);
console.log(cities.length);

let blankArr = [];
console.log(blankArr.length);

// Length property can also be used with strings. Some array method and properties can also be used with strings

let fullname = "Jamie Oliver";
console.log(fullname.length);

// Removing the last element in an array

myTask.length = myTask.length-1;
console.log(myTask.length);
console.log(myTask);

// 
// let city1 = "Tokyo";
// let city2 = "Manila";
// let city3 = "Jakarta";

// let cities = [city1, city2, city3];
// console.log(cities);

cities.length--;
console.log(cities.length);
console.log(cities);

// Same output because we cannot do the same on strings
console.log("Initial length of fullname: " + fullname.length);
fullname.length = fullname.length-1;
// console.log(fullname.length);
console.log("current length of fullname: " + fullname.length);
console.log(fullname);

let theBeatles = ['John', 'Paul', 'Ringo', 'George'];
theBeatles++;
console.log(theBeatles);

// Accessing the element of an array through index
// let grades = [98.5, 94.3, 89.2, 90.1, 99];
console.log(grades[2]);

// let computerBrands= ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu']
console.log(computerBrands[3]);

// let grades = [98.5, 94.3, 89.2, 90.1, 99];
function getGrade(index){
	console.log(grades[index]);
}
getGrade(3);
getGrade(grades.length-1);

console.log("---------------------------------------")
// [SECTION] Change of elements

let lakersLegend = ['Kobe', 'Shaq', 'Lebron', 'Magic', 'Kareem'];
let currentLaker = lakersLegend[2];
console.log(currentLaker);

// Change an element / reassigning array values
console.log('Array before reassignment');
console.log(lakersLegend);

lakersLegend[2] = 'Pau Gasul';
console.log('Array after reassignment')
console.log(lakersLegend);

// Change the last element
let bullsLegend = ['Jordan', 'Pipen', 'Rodman', 'Rose', 'Kukoc'];
console.log(bullsLegend);
let lastElementIndex = bullsLegend.length-1;
console.log(lastElementIndex);
console.log(bullsLegend[lastElementIndex]);
// You could also access it directly
console.log(bullsLegend[bullsLegend.length-1]);
bullsLegend[lastElementIndex] = "Harper";
console.log(bullsLegend);

console.log("---------------------------------------")
// [SECTION] Add items into the array

let newArr = [];
console.log(newArr[0]);

newArr[0] = "Cloud Strife";
console.log(newArr);

newArr[2] = "Tifa Lockhart";
console.log(newArr);

// Adding elements after the last element

newArr[newArr.length] = "Wallace Bayola";
console.log(newArr);

// Looping over an array (display per elements of an array)

for(let index=0; index<newArr.length; index++){
	console.log(newArr[index]); // 0, after increment // 1 // 2 // 3
}

let numArr = [5, 12, 30, 46, 40];

// for divisibilioty of numer use " '%'' = modulo"

// A loop that will check per element is divisible by 5
for(let index = 0; index<newArr; index++){
	if(numArr[index]% 5 === 0){
		console.log(numArr[index] + " is divisible by 5");
	}
	else{
		console.log(numArr[index] + " is not divisible by 5")
	}
}

// To separate elements use "',' - comma"
let chessBoard = [
	['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
	['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
	['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
	['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
	['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
	['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
	['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
	['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
	]

console.log(chessBoard);
					//row//col
console.log(chessBoard[1][4]); // e2

console.log(chessBoard[4][2]); // c5

console.log("Pawn moves to: " + chessBoard[1][5]);

// pag gagagmit ng function deretcho sa arugment then index numebrer

